const express = require('express');
const app = express();
const cors = require("cors")({ origin: true })
const { BigQuery } = require('@google-cloud/bigquery');
const firebase = require('firebase');
const bigquery = new BigQuery({
    projectId: 'tracking-253213',
    keyFilename: './tracking-7cb86f6d60f9.json'
});
app.use(cors)
app.get('/', (req, res) => {
    res.send('API working correctly');
});

var firebaseConfig = {
    apiKey: "AIzaSyDr9_7SFzs7rdrBAvD_zRFJ87SY7qeTrsg",
    authDomain: "tracking-253213.firebaseapp.com",
    databaseURL: "https://tracking-253213.firebaseio.com",
    projectId: "tracking-253213",
    storageBucket: "tracking-253213.appspot.com",
    messagingSenderId: "720057477734",
    appId: "1:720057477734:web:b18ff5ed68c377d23f5ba7",
    measurementId: "G-NP7981RXVB"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

app.get("/getLogisticsTrackBigQuery", async (req, res) => {
    // https://tracking-253213.appspot.com/getLogisticsTrackBigQuery?LIMIT=9000&COURIER=DS
    // let dataset = bigquery.dataset('OcTracking');
    // let table = dataset.table('trackingTest2');
    let request = req
    console.log("started getLogisticsTrackBigQuery")
    let query = 'SELECT * FROM `tracking-253213.OcTracking.trackingTest2`'
    if(req.query.hasOwnProperty('COURIER')){
        query = query + " WHERE COURIER = " + "\""+request.query.COURIER+"\""
    }

    if (req.query.hasOwnProperty('LIMIT')){
        query = query + ' LIMIT ' + parseInt(request.query.LIMIT) 
    } 

    
    bigquery.query(query).then((rows, err) => {
        if (!err) {
            console.log("success!")
            return res.status(200).json({
                status: 200,
                original_query: request.query,
                results: rows[0]
            })
        } else {

            console.log("err")
            console.log(err)
            return res.status(401).json({
                status: 401,
                msg: "error fetching data for logistics track OCs. 401",
                error: err
            })
        }
    }).catch(err => {
        console.log("ERROR ERROR")
        console.log(err)
        return res.status(400).json({
            status: 400,
            msg: "error fetching data for logistics track OCs 400",
            error: err
        })
    });
})

app.get("/returnBeer", async (req, res) => {
    // https://tracking-253213.appspot.com/returnBeer
    return res.status(200).json({
        status: 200,
        msg: "Beer"
    })
    
})

app.get("/getfirebaseOC", async (req, res) => {
    // https://tracking-253213.appspot.com/getfirebaseOC
    firebase.database().ref("tracking/123456789").once("value", function(snapshot) {
        return res.status(200).json({
            status: 200,
            msg: snapshot.val()
        })
      }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
      });
})

app.get("/writeOCComment", async (req, res) => {
    // https://tracking-253213.appspot.com/writeOCComment?OC=123456787
    // https://tracking-253213.appspot.com/writeOCComment?OC=123456787&internal_comment=Now is the time for all&user=test@test.com
    // http://tracking-253213.appspot.com/writeOCComment?OC=123456787&internal_comment=Now%20is%20the%20time%20for%20all&user=test@test.com&client_comment=good%20men%20to%20come%20to%20the%20aid%20of%20their%20country&OT=696481789836
    // http://tracking-253213.appspot.com/writeOCComment?OC=123456787&internal_comment=smurfing%20all%20day%20long&user=test@test.com&client_comment=Boo%20Radley%20and%20company&OT=696481789836
    let request = req
    console.log("started writeOCComment")

    if(!req.query.hasOwnProperty('OC')){
        return res.status(400).json({
            status: 400,
            msg: "Request for comment has no data"
        })
    }

    if(!req.query.hasOwnProperty('internal_comment') && !req.query.hasOwnProperty("client_comment")){
        return res.status(400).json({
            status: 400,
            msg: "Request for comment has no data"
        })
    }

    let OC = req.query.OC
    let OT = ""
    let DS = ""
    let Estado = ""
    let incident = ""
    let client_comment_obj = {}
    let internal_comment_obj = {}

    let timestamp = Date.now()

    if(req.query.hasOwnProperty("OT")){
        OT = req.query.OT
    }
    if(req.query.hasOwnProperty("DS")){
        DS = "/"+req.query.DS
    }

    if(req.query.hasOwnProperty("internal_comment")){
        internal_comment_obj["timestamp"]=timestamp
        internal_comment_obj["comment"]=req.query.internal_comment
        internal_comment_obj["by"]=req.query.user
        internal_comment_obj["Estado"]=req.query.Estado
        internal_comment_obj["incident"]=req.query.incident
    }

    if(req.query.hasOwnProperty("client_comment")){
        client_comment_obj["timestamp"]=timestamp
        client_comment_obj["comment"]=req.query.client_comment
        client_comment_obj["by"]=req.query.user
        client_comment_obj["Estado"]=req.query.Estado
        client_comment_obj["incident"]=req.query.incident
    }
    console.log(client_comment_obj)
    console.log(internal_comment_obj)
    if(OT==""){
        var newPostKey = firebase.database().ref("tracking/"+OC+DS).child('internal_comments').push(internal_comment_obj);
        var newPostKey2 = firebase.database().ref("tracking/"+OC+DS).child('client_comments').push(client_comment_obj);
        return res.status(200).json({
            status: 200,
            msg: newPostKey.key + ":" + newPostKey2.key
        })
    }
    else{
        firebase.database().ref("tracking/"+OC+"/OT_list").once("value", function(snapshot)
        {
            let OT_list = snapshot.val()
            for(i=0; i<OT_list.length; i++){
                if (OT_list[i].OT == OT){
                    // get current status
                    let status = OT_list[i].Estado
                    if (internal_comment_obj != {}){
                        internal_comment_obj["Estado"]=status
                    }
                    if (client_comment_obj != {}){
                        client_comment_obj["Estado"]=status
                    }
                    var newPostKey = firebase.database().ref("tracking/"+OC+"/OT_list/"+i+"/").child('internal_comments').push(internal_comment_obj);
                    var newPostKey2 = firebase.database().ref("tracking/"+OC+"/OT_list/"+i+"/").child('client_comments').push(client_comment_obj);
                    return res.status(200).json({
                        status: 200,
                        msg: newPostKey.key + ":" + newPostKey2.key
                    })
                }
            }

        });
    }
});



const server = app.listen(8080, () => {
    const host = server.address().address;
    const port = server.address().port;

    console.log(`Example app listening at http://${host}:${port}`);
});